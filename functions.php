<?php
declare(strict_types=1);

// 1.	Napište rekurzivní anebo nerekurzivní (anebo obě) funkci na výpočet faktoriálu.

function factorialRecursive(int $number): int
{
    if ($number < 2) {
        return 1;
    }

    return factorialRecursive($number - 1) * $number;
}

function factorial(int $number): int
{
    if ($number < 2) {
        return 1;
    }

    $factorial = 1;
    for ($number; $number >= 1; $number--) {
        $factorial = $factorial * $number;
    }

    return $factorial;
}

// 2. Napište v PHP kód, který obrátí pořadí slov ve větě a vypíše výsledek. Na vstupu bude například věta “TOTO JE TEST Z PHP” 
// A výstupem bude “PHP Z TEST JE TOTO”

function reverseSentence(string $text): string
{
    $array = array_reverse(explode(" ", $text));

    return implode(" ", $array);
}

echo reverseSentence("TOTO JE TEST Z PHP");

// 3.	Co vypíše příkaz pro proměnné $age = 5 a $score = 12:

// Tady mi chybí info o tom, jaký příkaz je myšlen. Nebo jsem nepochopil otázku...

// 4.	Napište co nejefektivnější algoritmus, který zjistí, jestli se v poli nachází alespoň jedno stejné číslo. 
//Pole může obsahovat čísla od 1 do 100 a může mít libovolný počet prvků.

function doesArrayContainDuplicates(array $array): bool
{
    if (count($array) !== count(array_unique($array))) {
        return true;
    }

    return false;
}

// 5. Jak uložím proměnnou do SESSION?
session_start();
$_SESSION["key"] = $myVariable;

// 6.	Uspořádání pole objektů

function compare(CPerson $person1, CPerson $person2): int
{
    return $person1->age <=> $person2->age;
}

usort($array, "compare");

// 7.	Jaký je rozdíl mezi operátory “=”, “==”, “===” v PHP

// = - Přiřadí hodnotu proměnné.
// == - Operátor porovnání, který ale neřeší datavé typy. Tedy ("1" == 1) vratí TRUE
// === - Operátor porovnávání, který ale ber v potaz datové typy. Tedy ("1" ==== 1) vratí FALSE

// 8. Zadefinujte abstraktní třídu a napište její příklad.

abstract class Mammal
{
    public $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    abstract public function intro(): string;
}

class Elephant extends Mammal
{
    public function intro(): string
    {
        return "Hi, I am " . $this->name . " and I have a very long trunk.";
    }
}

class Giraffe extends Mammal
{
    public function intro(): string
    {
        return "Hi, I am " . $this->name . " and I have a  very long neck.";
    }
}

$elephantPetr = new Elephant("Petr");
$giraffeMarketa = new Giraffe("Markéta");

// 9.	Mějme pole čísel. Toto pole čísel upravme tak, že klíče vyměníme za prvky. 
//V případě, že v původním poli byly 2 stejné prvky, tak danému klíči přiřadíme po výměně pole hodnot.

function flipArray(array $array): array
{
    $fliped = array_flip($array);

    foreach ($fliped as $key => $value) {
        if (count($valuesForKey = array_keys($array, $key)) > 1) {
            $fliped[$key] = $valuesForKey;
        }
    }

    return $fliped;
}

// 10. Co znamená, když funkci předávám hodnotu referencí a jak se to v PHP dělá?

// Funkci předávám referenci, tedy "přímo odkaz" na místo, kde je v paměti proměnná uložena.
// Pokud změním hodnotu proměnné v běhu funkce, změní se hodnota promměné i mimo funkci.

function magic(&$param)
{
    $param = 8;
}

$param = 5;
magic($param);
echo $param; // 8

-- 1.	Vypište seznam studentů seřazených podle křestního jména (vzestupně) a v rámci křestního jména podle příjmení (sestupně).
SELECT *
FROM STUDENT
ORDER BY FIRST_NAME ASC, LAST_NAME DESC;

-- 2.	Vypište jen jeden záznam – nejstaršího studenta. 
SELECT *
FROM STUDENT
ORDER BY DATE_OF_BIRTH ASC
LIMIT 1;

-- 3.	Vypište seznam studentů tak, že výsledný řádek bude obsahovat ID, Jméno, Příjmení a Počet bodů z matematiky daného studenta.
SELECT s.id, s.FIRST_NAME, s.LAST_NAME, SUM(p.POINTS)
FROM STUDENT s

JOIN POINTS_MATH p
ON s.ID = p.PERSON_ID

GROUP BY s.id

-- 4.	Upravte query z bodu (3) takovým způsobem, že bude obsahovat i studenty, kteří nemají žádné body z matematiky.
SELECT s.id, s.FIRST_NAME, s.LAST_NAME, SUM(IFNULL(p.POINTS, 0)) as POINTS
FROM STUDENT s

LEFT JOIN POINTS_MATH p
ON s.ID = p.PERSON_ID

GROUP BY s.id

-- 5.	Napište, jaký index musí být přidán, aby byla query z otázek 3 a 4 efektivní.
-- Přidal bych index pro s.ID a p.PERSON_ID (CREATE INDEX ...)

-- 6.	Vypište seznam studentů tak, že příjmení bude vypsané velkými písmeny a v posledním sloupci RESULT SETU bude délka příjmení.
SELECT 
  ID, 
  FIRST_NAME, 
  UPPER(LAST_NAME) AS LAST_NAME, 
  DATE_OF_BIRTH, 
  CLASS, 
  LENGTH(LAST_NAME) AS LAST_NAME_LENGTH
FROM STUDENT;

-- 7.	Vypište seznam studentů a do posledního sloupce přidejte věk v rocích.
-- Pozn. query jsem testoval na MariaDB. Pro jistotu uvádím, protože funkce pro práci s časem se občas lehce liší u jednotlivých databázových systémů, např. DATEDIFF u MariaDB bere pouze dva argumenty (vždy vrací rozdíl ve dnech) - musel bych tedy použít lehce komplikovaněji - viz druhý SELECT
SELECT 
  *,
  TIMESTAMPDIFF(YEAR, DATE_OF_BIRTH, NOW()) AS AGE
FROM STUDENT;

SELECT 
  *,
  FLOOR(DATEDIFF(NOW(), DATE_OF_BIRTH) / 365) AS AGE
FROM STUDENT;


-- 8.	Jaký příkaz bych použil, kdybych chtěl zjistit, jaké indexy se použily na dané query
-- EXPLAIN SELECT...

-- 9.	Spočítej průměrný počet bodů z matematiky na jednoho žáka pro každou třídu.
-- Přistupuji k tomu, tak že všichni studenti z tabulky STUDENT studují daný předmět
-- Raději jsem ošetřil, aby se nikdy nedělal SUM nad NULL hodnotou a neprovádělo se dělení 0
-- Pro lepší čtení jsem pak rozdělil do dvou selectů, šlo by i v jednom
SELECT

  CLASS,
  IF(POINTS = 0, 0, POINTS / STUDENT_COUNT) AS POINTS_AVERAGE

FROM (

  SELECT 
    s.CLASS,
    SUM( IFNULL(p.POINTS, 0) ) AS POINTS,
    COUNT( DISTINCT s.ID ) AS STUDENT_COUNT

  FROM STUDENT s

  LEFT JOIN POINTS_MATH p
  ON p.PERSON_ID = s.ID

  GROUP BY s.CLASS

) s;

-- 10.	Pomocí jedné query, anebo sadou query vrať seznam studentů a pro každého sumu bodů z fyziky a matematiky.
-- V mém případě místo fyziky dějepis
-- Zkusil jsem v jedné query
SELECT 
  ID, 
  FIRST_NAME, 
  LAST_NAME, 
  SUM(CASE SUBJECT WHEN 'history' THEN POINTS ELSE 0 END) as HISTORY_POINTS,
  SUM(CASE SUBJECT WHEN 'math' THEN POINTS ELSE 0 END) AS MATH_POINTS

FROM (

  SELECT 
    s.ID,
    s.FIRST_NAME,
    s.LAST_NAME,
    s.CLASS,
    IFNULL(h.POINTS, 0) AS POINTS,
    "history" as SUBJECT

  FROM STUDENT s
  LEFT JOIN POINTS_HISTORY h
  ON s.ID = h.PERSON_ID

  UNION ALL

  SELECT 
    s.ID,
    s.FIRST_NAME,
    s.LAST_NAME,
    s.CLASS,
    IFNULL(m.POINTS,0) AS POINTS,
    "math" as SUBJECT

  FROM STUDENT s
  LEFT JOIN POINTS_MATH m
  ON s.ID = m.PERSON_ID

) s

GROUP BY ID;